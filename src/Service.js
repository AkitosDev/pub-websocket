import C_IpAddress from './valueObjects/IpAddress';
import C_Port      from './valueObjects/Port';
import C_Server    from './webSocket/Server';

const IpAddress = new C_IpAddress('127.0.0.1');
const Port      = new C_Port(8000);

const Server = new C_Server(IpAddress, Port);

Server.run();