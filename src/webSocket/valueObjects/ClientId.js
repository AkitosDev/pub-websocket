/**
 * @class ClientId
 * @param clientId
 */
export default function ClientId(clientId)
{
    /**
     * @private
     * @type {string}
     */
    var _clientId;

    /**
     * @private
     * @constructor
     */
    const _constructor = () =>
    {
        _ensureClientIdIsString();
        _ensureClientIdHasValidLength();

        _clientId = clientId;
    };

    /**
     * @private
     */
    const _ensureClientIdIsString = () =>
    {
        if(typeof(clientId) !== 'string') {
            console.log('clientId has to be a string');

            process.exit();
        }
    };

    /**
     * @private
     */
    const _ensureClientIdHasValidLength = () =>
    {
        if(clientId.length !== 25) {
            console.log('clientId must have a length of 25');

            process.exit();
        }
    };

    _constructor();

    /**
     * @public
     * @returns {string}
     */
    this.asString = () =>
    {
        return _clientId;
    }
}
