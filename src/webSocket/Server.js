import ws from 'ws';

import C_ClientIdGenerator from './module/ClientIdGenerator';

/**
 * @class Server
 * @param IpAddress {IpAddress}
 * @param Port {Port}
 */
export default function Server(IpAddress, Port)
{
    /**
     * @private
     * @type {Array}
     */
    var _connections;

    /**
     * @private
     * @type {WebSocketServer}
     */
    var _WebSocketServer;

    /**
     * @private
     * @constructor
     */
    const _constructor = () =>
    {
        _connections = [];

        _WebSocketServer = new ws.Server
        (
            {
                host: IpAddress.asString(),
                port: Port.asNumber()
            }
        );

        console.log('Server started on ' + IpAddress.asString() + ':' + Port.asNumber());
    };

    _constructor();

    /**
     * @public
     */
    this.run = () =>
    {
        _WebSocketServer.on('connection', (connection) =>
            {
                const ClientId = new C_ClientIdGenerator()
                    .getNewClientId();

                registerConnection(connection, ClientId);

                connection.on('message', (message) =>
                    {
                        console.log(message);
                    }
                );

                connection.on('close', () =>
                    {
                        unregisterConnection(ClientId);
                    }
                );
            }
        );
    };

    /**
     * @private
     * @param connection
     * @param ClientId {ClientId}
     */
    const registerConnection = (connection, ClientId) =>
    {
        _connections[ClientId.asString()] = connection;

        console.log('new connection  - clientID: ' +
            ClientId.asString()
        );
    };

    /**
     * @private
     * @param ClientId {ClientId}
     */
    const unregisterConnection = (ClientId) =>
    {
        delete _connections[ClientId.asString()];

        console.log('connection lost - clientID: ' +
            ClientId.asString()
        );
    }
}
