import C_MyMath   from '../../module/MyMath';
import C_ClientId from '../valueObjects/ClientId';

/**
 * @class ClientIdGenerator
 */
export default function ClientIdGenerator()
{
    /**
     * @public
     * @returns {ClientId}
     */
    this.getNewClientId = () =>
    {
        var clientId = new Date()
            .getTime()
            .toString();

        var i = 0;

        for(i; i < 12; i++) {
            clientId = clientId +
                new C_MyMath().getRandomNumberOutOf(10);
        }

        return new C_ClientId(clientId);
    }
}
