/**
 * @class Port
 * @param port {number}
 */
export default function Port(port)
{
    /**
     * @private
     * @type {number}
     */
    var _port;

    /**
     * @private
     * @constructor
     */
    const _constructor = () =>
    {
        _ensurePortIsNumber();
        _ensurePortHasAValidRange();

        _port = port;
    };

    /**
     * @private
     */
    const _ensurePortIsNumber = () =>
    {
        if(typeof(port) !== 'number') {
            console.log('port is not a number');

            process.exit();
        }
    };

    /**
     * @private
     */
    const _ensurePortHasAValidRange = () =>
    {
        if(port < 1 || 65535 < port) {
            console.log('Invalid port range');

            process.exit();
        }
    };

    _constructor();

    /**
     * @public
     * @returns {number}
     */
    this.asNumber = () =>
    {
        return _port;
    }
}
