/**
 * @class IpAddress
 * @param ipAddress {string}
 */
export default function IpAddress(ipAddress)
{
    /**
     * @private
     * @type {string}
     */
    var _ipAddress;

    /**
     * @private
     * @constructor
     */
    const _constructor = () =>
    {
        _ensureIpAddressIsValid();

        _ipAddress = ipAddress;
    };

    /**
     * @private
     */
    const _ensureIpAddressIsValid = () =>
    {
        let regex = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;

        if(!regex.test(ipAddress)) {
            console.log('ipAddress is not a valid ip address');

            process.exit();
        }
    };

    _constructor();

    /**
     * @public
     * @returns {string}
     */
    this.asString = () =>
    {
        return _ipAddress;
    }
}
