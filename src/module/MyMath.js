/**
 * @class MyMath
 */
export default function MyMath()
{
    /**
     * @public
     * @param amount {number}
     * @returns {number}
     */
    this.getRandomNumberOutOf = (amount) => {
        return Math.floor(Math.random() * amount);
    }
}
