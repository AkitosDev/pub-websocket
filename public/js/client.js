// WIP, hasn't been refactored yet

const client = Object.create(null);

client.webSocket = null;

client.connect = function () {
    client.webSocket = new WebSocket("ws://127.0.0.1:8000");

    client.webSocket.onopen = function () {
        console.log('connected');
    };

    client.webSocket.onmessage = function (event) {

    };

    client.webSocket.onclose = function () {
        console.log('disconnected');
    }
};

client.sendStringToServer = function (string) {
    if(typeof(string) === 'string') {
        client.webSocket.send(string);
    }
};

client.callAServerMethod = function (objectName, methodName, array) {
    if(typeof(objectName) === 'string' && typeof(methodName) === 'string' && typeof(array) === 'object')  {
        array.unshift(objectName, methodName);

        client.webSocket.send(JSON.stringify(array));
    }
};