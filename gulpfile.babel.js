'use strict';

var gulp    = require('gulp');
const babel = require('gulp-babel');

gulp.task('transpile', function() {

    gulp.src('src/*.js')
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest('build/src/'));

    gulp.src('src/module/*.js')
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest('build/src/module'));

    gulp.src('src/valueObjects/*.js')
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest('build/src/valueObjects'));

    gulp.src('src/webSocket/*.js')
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest('build/src/webSocket'));

    gulp.src('src/webSocket/module/*.js')
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest('build/src/webSocket/module'));

    gulp.src('src/webSocket/valueObjects/*.js')
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest('build/src/webSocket/valueObjects'));

    gulp.src('public/html/*.html')
        .pipe(gulp.dest('build/public/html/'));

    gulp.src('public/js/*.js')
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest('build/public/js/'));

});

gulp.task('build', ['transpile']);